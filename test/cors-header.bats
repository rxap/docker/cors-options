#!/usr/bin/env bats

load ./load.bash

@test "should response with default allow methods header" {
  run http --print=h OPTIONS "$baseUrl/cors-test"
  assert_success
  assert_output --partial "Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS"
}

@test "should response with default allow origin header" {
  run http --print=h OPTIONS "$baseUrl/cors-test"
  assert_success
  assert_output --partial "Access-Control-Allow-Origin: *"
}

@test "should response with default allow headers header" {
  run http --print=h OPTIONS "$baseUrl/cors-test"
  assert_success
  assert_output --partial "Access-Control-Allow-Headers: sentry-trace, baggage, Accept, Accept-Language, Content-Language, Content-Type, Authorization, Cache-Control, If-Modified-Since, X-Requested-With, X-Forwarded-For, X-Real-IP, X-Forwarded-Proto, X-Forwarded-Host, X-Forwarded-Port, DNT, User-Agent, Keep-Alive, Origin"
}

@test "should response with default allow credentials header" {
  run http --print=h OPTIONS "$baseUrl/cors-test"
  assert_success
  assert_output --partial "Access-Control-Allow-Credentials: true"
}

@test "should response with 204 status for OPTIONS request" {
  run http --print=h OPTIONS "$baseUrl/cors-test"
  assert_success
  assert_output --partial "HTTP/1.1 204 No Content"
}

@test "should response with 400 status for non OPTIONS request" {
  run http --print=h GET "$baseUrl/cors-test"
  assert_success
  assert_output --partial "HTTP/1.1 400 Bad Request"
}



