#!/bin/bash

BASE_DIR=$(git rev-parse --show-toplevel)

cd "$BASE_DIR" || exit 1

docker build --no-cache -q -t cors-options:test .

docker run -d -p 8787:80 cors-options:test
