ARG NGINX_TAG=alpine

FROM nginx:${NGINX_TAG}

LABEL traefik.enable="true"
LABEL traefik.http.routers.rxap-cors-options.rule="Method(`OPTIONS`)"
LABEL traefik.http.routers.rxap-cors-options.priority="9999"

ENV ALLOW_ORIGIN="*"
ENV ALLOW_METHODS="GET, POST, PUT, DELETE, OPTIONS"
ENV ALLOW_HEADERS="sentry-trace, baggage, Accept, Accept-Language, Content-Language, Content-Type, Authorization, Cache-Control, If-Modified-Since, X-Requested-With, X-Forwarded-For, X-Real-IP, X-Forwarded-Proto, X-Forwarded-Host, X-Forwarded-Port, DNT, User-Agent, Keep-Alive, Origin"
ENV ALLOW_CREDENTIALS="true"

COPY default.conf.template /etc/nginx/templates/default.conf.template
